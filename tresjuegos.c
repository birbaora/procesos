
/* Este es un ejemplo básico de creación de procesos en el que el padre y el hijo
actúan sobre sus variables de forma independiente, lo que demuestra que los procesos son clones, cada uno con su pila y sus datos (aunque pudieran compartir código) */

#include <iostream>
#include <string>
 
// Required by for routine
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
    
#include <stdlib.h>   // Declaration for exit()
    
using namespace std;

char * lineas[3]={ 
	"/usr/games/gnome-mines",
	"/usr/games/gnome-sudoku",
	"/usr/games/gnome-mahjongg" };
    
main()
    {
	int num=0 ;
	pid_t  pID;
	pid_t pID0, pID1, pID2;
	while (num < 3 ) {       
	       if (num == 0) pID = pID0 = fork();
	       if (num == 1) pID = pID1 = fork();
	       if (num == 2) pID = pID2 = fork();

	       if (pID == 0) {
		 char *newargv[] = { NULL,
				    "hello",
				    "world",
				     NULL };
       		 char *newenviron[] = { "DISPLAY=:0.0",NULL };
       		 execve(lineas[num],newargv,newenviron );
		} 
    		num++;
	}

	pid_t pID_fin;
	int estado;
	pID_fin = wait(&estado);
	if (pID_fin == pID0) cout << "correcto el 1"<< endl;
	pID_fin = wait(&estado);
	if (pID_fin == pID1) cout << "correcto el 2"<< endl;
	pID_fin = wait(&estado);

        if (pID_fin == pID2)
	 cout<<"El último en nacer fue el último en finalizar" << endl;
	else
	 cout<<"El último en nacer murió Antes de hora" << endl;
        exit(0);
    }




