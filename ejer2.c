//#include <iostream>
#include <stdio.h>
//#include <string>

#include <sys/types.h>
#include <unistd.h>
    
#include <stdlib.h>   // Declaration for exit()
    
//using namespace std;

int main(int argc, char * argv[])
    { pid_t pID ;

       printf("Inicio\n");
       pID = fork();
       printf("fork() hecho, pid recibido %d \n",pID);

       printf("¿Soy elpadre o el hijo? \n");
       exit(0);
    }


