// lanzar un programa repetidamente hasta que éste termine bien


// Required by for routine
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include <iostream>    
#include <stdlib.h>   // Declaration for exit()
    
using namespace std;
    
int main(int argc,char * argv[])
    {
 	
 	pid_t pID = 1 ;
 	for (int i=0; i<3000 ; i++ ) {
 	// while (true) {
         pID = fork();
         if (pID == 0 ) {
         
              char *newargv[] = { NULL, "hello", "world", NULL };
              char *newenviron[] = { "DISPLAY=:0.0",NULL };

         	execve("/usr/bin/galculator",newargv,newenviron);
         	cout << "Soy el hijo "  << i << endl;
         	exit(0);
         }
         int estado;
         wait(&estado);
         
         	if( estado == 0 ) break;
 	} //for
 
    cout << "Un hijo ha muerto bien, por fin. Doy la tarea por terminada" << endl;
 	exit(0);
}

