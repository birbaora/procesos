
/* Este es un ejemplo básico de creación de procesos en el que el padre y el hijo
actúan sobre sus variables de forma independiente, lo que demuestra que los procesos son clones, cada uno con su pila y sus datos (aunque pudieran compartir código) */

#include <iostream>
#include <string>
 
// Required by for routine
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
    
#include <stdlib.h>   // Declaration for exit()
    
using namespace std;
    
    
main()
    {

       cout << "Mensaje 1 " << endl;
       pid_t  pID;
       pID = fork();

       if (pID == 0){
		sleep(2);
       		 cout << "soy el proceso hijo" << endl;

		exit(1);
}
       else {
          cout << "Soy el proceso padre"<< endl;
	  pid_t pID_Hijo;
          int estado=8;
          pID_Hijo = wait(&estado);
	  cout << "mi hijo "<< pID_Hijo << "  ha terminado con estado "<<
		 WEXITSTATUS(estado) << endl;

	  if (pID_Hijo == pID ) 
		cout << "El hijo finalizado sí que era mi hijo"<< endl;
       }
     
        exit(0);
    }




