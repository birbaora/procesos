
/* Este es un ejemplo básico de creación de procesos en el que el padre y el hijo
actúan sobre sus variables de forma independiente, lo que demuestra que los procesos son clones, cada uno con su pila y sus datos (aunque pudieran compartir código) */

#include <iostream>
#include <string>
 
// Required by for routine
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
    
#include <stdlib.h>   // Declaration for exit()
    
using namespace std;
    
int globalVariable = 2;
    
main()
    {
       string identificadorProceso;
       int  variablePila = 20 ;
 

       pid_t pID ;
      variablePila=40;
       pID = fork();
       if (pID == 0)                // child
       {
          // Code only executed by child process  
         identificadorProceso = "Child Process: ";
          globalVariable++;
          variablePila++;
        } else     // parent
        {
          // Code only executed by parent process
	  	variablePila=60;
          identificadorProceso = "Parent Process:";
		sleep(1);
        } 
        // Code executed by both parent and child.
      
        cout << identificadorProceso ;
        cout << " Variable Global: " << globalVariable;
        cout << " Variable en la Pila: "  << variablePila << endl;

    exit(0);
    }




