// lanzar tres ventanas simultáneamente (se espera sólo a que termine una)


// Required by for routine
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include <iostream>    
#include <stdlib.h>   // Declaration for exit()
    
using namespace std;
    
int globalVariable = 2;

char * cadenas[]={"/usr/games/gnome-mines","/usr/games/ltris","/usr/bin/galculator"};
    
int main(int argc,char * argv[])
    {
 	
 	pid_t pID = 1 ;
 	for (int i=0; i<3 ; i++ ) {
 	
         pID = fork();
         if (pID == 0 ) {
         
              char *newargv[] = { NULL, "hello", "world", NULL };
              char *newenviron[] = { "DISPLAY=:0.0",NULL };

         	execve(cadenas[i],newargv,newenviron);
         	cout << "Soy el hijo "  << i << endl;
         	exit(0);
         }
         int estado;
         wait(&estado);
 	} //for
 
 int estado;
 wait(&estado);
 	exit(0);
}

